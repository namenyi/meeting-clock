var elapsedTime = 0;
var timeoutFlag = 0;
var startTime = 0;
var oldTime = 0;
var theTimeout;

var timeLbl = $('[name="time"]');
var costLbl = $('[name="cost"]');
var countInp = $('[name="count"]');
var rateInp = $('[name="rate"]');

$('#startBtn').click(function () {
    ds = new Date();
    startTime = ds.getTime();
    startClock();
});

$('#pauseBtn').click(function () {
    oldTime = elapsedTime;
    if (timeoutFlag == 1) {
        clearTimeout(theTimeout);
    }
});

$('#resetBtn').click(function () {
    resetClock();
});

function startClock() {
    dc = new Date();
    current_time = dc.getTime();
    elapsedTime = oldTime + current_time - startTime;

    hours = Math.floor(elapsedTime / 3600000);
    minutes = Math.floor((elapsedTime / 60000) - (hours * 60));
    seconds = Math.floor((elapsedTime / 1000) - (hours * 60 * 60) - (minutes * 60));
    timeStr = "" + hours;
    timeStr += ((minutes < 10) ? ":0" : ":") + minutes;
    timeStr += ((seconds < 10) ? ":0" : ":") + seconds;
    timeLbl.val(timeStr);

    elapsed_seconds = elapsedTime / 1000;
    cost_per_second = (countInp.val() * rateInp.val()) / (60 * 60);
    rounded_cost = Math.round(elapsed_seconds * cost_per_second * 100) / 100;
    costLbl.val((rounded_cost) + ' Ft');

    timeoutFlag = 1;
    theTimeout = setTimeout('startClock();', 1000);
}

function resetClock() {
    if (timeoutFlag == 1) {
        clearTimeout(theTimeout);
    }
    oldTime = 0;
    costLbl.val("0.00 Ft");
    timeLbl.val("0:00:00");
}