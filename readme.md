# Meeting költség mérő

Egy időmérő és kalkulátor, ami mutatja, hogy a megbeszélés résztvevőinek fizetése alapján mekkora költséggel járt az eddig eltelt idő alatt a megbeszélés.

> They say time is money.  Did you ever sit in a meeting or class and wonder how much money was represented by the time of all the people sitting there?  The Meeting Clock will keep track for you!  The Meeting Clock is something I created for fun.  It's a 100% client-side web application that you can download and run on any PC, laptop PC, Mac, Handheld PC, or anything else that has a JavaScript capable web browser.  NEW: You can also bookmark it and run it from any smart phone (android, iPhone, Blackberry, etc.)  A version without this text, just the clock application, formatted for smart phones can be accessed here.

![Képernyő felvétel](screenshot.png)

Bootstrap v3.3.6 és Bootstrap Cover template alapján

## Copyright notice

Az eredeti verzió, aminek felhasználásával készült: https://www.mcgurrin.com/clock.htm

> © 2000 by Michael McGurrin.  May be freely redistributed provided copyright notice remains unchanged.May not be sold, redistributed for a fee, or sold as part of a software bundle without prior permission of author